	.data
fname:	.asciiz "blank.bmp"		# input file name
outfn:	.asciiz "result.bmp"	
imgInf:	.word 512,512,pImg,0,0,0 #struct:w,h,pointer to the image buffer,cX,cY,colour:0-b,1-w
handle: .word 0
fsize:	.word 0

pFile:	.space 62 #190 bytes - size of "small.bmp" - 128 bytes(32x32/8) = 62 bytes.
pImg:	.space 36000

read_input_message: .asciiz "\nPlease enter initial coordinates (x,y) \n" 
read_input_message2: .asciiz "Please enter final (x,y) coordinates\n" 
final_message: .asciiz "Done!:)\n"


	.text
main:	
	# open input file for reading
	# the file has to be in current working directory
	# (as recognized by mars simulator)
	la $a0, fname
	li $a1, 0
	li $a2, 0
	li $v0, 13
	syscall
	
	move $a0, $v0 
	sw $a0, handle
	la $a1, pFile #a1 - adress of input buffer (62 bytes)
	la $a2, 36062 #maximum number of characters to read
	li $v0, 14  #14 - read from file
	# $v0 contains number of characters read (0 if end-of-file, negative if error)
	syscall
	
	# store file size for further use and print it
	move $a0, $v0 #a- now contains number of characters read
	sw $a0, fsize #store this number in fsize .word
	li $v0, 1     #print integer in $a0
	syscall
	
	# close file
	li $v0, 16
	syscall
	
###########black square in the lower left corner
li $a2,0 #count for lines = height /2 
li $a1,0 #count for bytes = width/8*2
la $a0,pImg

set_black_down_left:
sb $zero,($a0)
addi $a1,$a1,1
addi $a0,$a0,1
blt $a1,32,set_black_down_left
addi $a2,$a2,1
add $a0,$a0,$a1
li $a1,0
blt $a2,256,set_black_down_left

###########black square in the right up corner
li $a2,0 #count for lines = height /2 
li $a1,0 #count for bytes = width/8*2
la $a0,pImg

addi $a0,$a0,16416

set_black_upper_right:
sb $zero,($a0)
addi $a1,$a1,1
addi $a0,$a0,1
blt $a1,32,set_black_upper_right
addi $a2,$a2,1
add $a0,$a0,$a1
li $a1,0
blt $a2,256,set_black_upper_right

#...................drawing checkboard.............................#


la $s0,imgInf  #storing adress of struct in s0
 
#register a1 stores the x coordinate
#register a2 stores the y coordinate

 #.......Some Horizontal Lines
 
 	li $a1,0
 	li $a2,20
	jal MoveTo
	li $a1,511
 	li $a2,20
        jal LineTo

	li $a1,0
 	li $a2,480
	jal MoveTo
	li $a1,511
 	li $a2,480
        jal LineTo

#....Some Vertical Lines..
	li $a1,40
 	li $a2,0
	jal MoveTo
	li $a1,40
 	li $a2,511
        jal LineTo
	
	li $a1,470
 	li $a2,0
	jal MoveTo
	li $a1,470
 	li $a2,511
        jal LineTo

 #....some diagonal lines below...

	li $a1,255
 	li $a2,255
	jal MoveTo
	li $a1,200
 	li $a2,80
        jal LineTo

	li $a1,255
 	li $a2,255
	jal MoveTo
	li $a1,80
 	li $a2,200
        jal LineTo

	li $a1,255
 	li $a2,255
	jal MoveTo
	li $a1,310
 	li $a2,80
        jal LineTo

	li $a1,255
 	li $a2,255
	jal MoveTo
	li $a1,430
 	li $a2,200
        jal LineTo

	li $a1,255
 	li $a2,255
	jal MoveTo
	li $a1,80
 	li $a2,310
        jal LineTo
	
	li $a1,255
 	li $a2,255
	jal MoveTo
	li $a1,200
 	li $a2,430
        jal LineTo
	
	li $a1,255
 	li $a2,255
	jal MoveTo
	li $a1,431
 	li $a2,310
        jal LineTo
	
	li $a1,255
 	li $a2,255
	jal MoveTo
	li $a1,310
 	li $a2,430
        jal LineTo


	li $a1,0
 	li $a2,0
	jal MoveTo
	li $a1,511
 	li $a2,511
        jal LineTo
	
	li $a1,0
 	li $a2,511
	jal MoveTo
	li $a1,511
 	li $a2,0
        jal LineTo
	

 ######################################

	# open the result file for writing
	la $a0, outfn
	li $a1, 1
	li $a2, 0
	li $v0, 13
	#result: $v0 contains file descriptor (negative if error). See note below table
	syscall
	
	# print handle of the file 
	move $a0, $v0
	sw $a0, handle
	li $v0, 1
	syscall
	
	# save the file (file size is restored from fsize)
	la $a1, pFile #$a1 = address of output buffer
	lw $a2, fsize #$a2 = number of characters to write
	li $v0, 15
	# as result: $v0 contains number of characters written (negative if error). See note below table
	syscall
	
	# close file
	li $v0, 16
	syscall
	
	#end	
	li $v0, 10
	syscall
	
	
SetColor:

# register $a1 holds the color 

    sgtu $a1, $a1, $zero  #if a1 is greater than 0 set a1 to a1
    sw $a1, 20($s0)
    
    
    la $v0, ($s0)
    jr $ra		
	
MoveTo:
# $a1 = x
# $a2 = y

    lw $t0, 0($s0)      # width of image (512 pixels)
    lw $t1, 4($s0)      # height of image (512 pixels)
    
    #checking boundaries 
    bltz $a1, return
    bge $a1, $t0, return
    bltz $a2, return
    bge $a2, $t1, return
    
    #setting structure variables Cx and Cy to desired coordinates,which were passed
    #to the function
    sw $a1, 12($s0) 
    sw $a2, 16($s0) 
    
return:
    la $v0, ($s0)
    jr $ra


SetPixel:
# $a1 = x
# $a2 = y

    lw $t0, 0($s0)      # taking current value of width of the image (512 pixels)
    #checking boundaries 
    bltz $a1, return2
    bge $a1, $t0, return2
    
    lw $t0, 4($s0)      # taking current value of hight of the image (512 pixels)
    #checking boundaries 
    bltz $a2, return2
    bge $a2, $t0, return2
    
    #translating the function in C
    add $t0, $t0, 31    # width + 31
    srl $t0, $t0, 5     # (width + 31) >> 5
    sll $t0, $t0, 2     # ((width + 31) >> 5) << 2
    mul $t0, $t0, $a2   # (((width + 31) >> 5) << ) * y
    srl $t1, $a1, 3     # x >> 3
    add $t0, $t0, $t1   # (((pImg->width + 31) >> 5) << 2) * y + (x >> 3)
    lw $t1, 8($s0)      # pImg->pImg
    add $t0, $t0, $t1  # pImg->pImg + (((pImg->width + 31) >> 5) << 2) * y + (x >> 3)
    
    li $t1, 0x80
    
    addi $sp,$sp,-4
    sw $a1,0($sp)
    
    and $a1, $a1, 7  #(x & 0x07)
    srlv $t1, $t1, $a1  # mask = 0x80 >> (x & 0x07)
    
   
    
    lbu $a1, ($t0)      # pixel value
    
    xor $t1,$t1,$a1
    
    lw $a1,0($sp)
    addi $sp,$sp,4
    
    b store_and_return
    
store_and_return:
    sb $t1, ($t0)       # store pixel value back
    jr $ra

return2:
    jr $ra
    
    
 LineTo:  #89 lines (including spaces),prev version:144 lines (including spaces)
#a1 - x - final coordinates
#a2 - y - final coordinates

addi $sp,$sp,-4
sw $ra,0($sp)

lw $t0,12($s0) #taking current CX coordinate
lw $t1,16($s0) #taking current CY coordinate

sub $t2,$a1,$t0  #dx 
abs $t2,$t2      #absolute value of dx

sub $t3,$a2,$t1  #dy 
abs $t3,$t3		#absolute value of dy

slt $t4,$t0,$a1  #sx = x0<x1? 1 : -1
beqz $t4,x0_notlessThan_x1 #xo is greater
here:
slt $t5,$t1,$a2  #sy= y0<y1? 1 : -1
beqz $t5,y0_notlessThan_y1 #yo is greater
here2:
#dx>dy ? (dx : -dy)/2
bgt $t2,$t3,dx_is_greater  
mul $t6,$t3,-1			
div $t6,$t6,2
#$t6 keeps error

loop:
addi $sp,$sp,-16
sw $a1,0($sp)
sw $a2,4($sp)
sw $t0,8($sp)
sw $t1,12($sp)
move $a1,$t0
move $a2,$t1
jal SetPixel
lw $a1,0($sp)
lw $a2,4($sp)
lw $t0,8($sp)
lw $t1,12($sp)
addi $sp,$sp,16

beq $t0,$a1,compare  #x0 == x1? if yes - checking y0 == y1
here3:
move $t7,$t6  #e2
#saving dx on the stack
addi $sp,$sp,-4
sw $t2,0($sp)

mul $t2,$t2,-1 #-dx
bgt $t7,$t2,e2_is_greater_mindx  #e2>-dx?
here4:
#pop dx value from stack
lw $t2,0($sp)
addi $sp,$sp,4
blt $t7,$t3,e2_is_less_dy   #e2<dy?
b loop

compare:
beq $t1,$a2,exit
b here3

x0_notlessThan_x1:
addi  $t4,$t4,-1
b here

y0_notlessThan_y1:
addi $t5,$t5,-1
b here2

dx_is_greater:
move $t6,$t2
div $t6,$t6,2
b loop

e2_is_greater_mindx:
sub $t6,$t6,$t3
add $t0,$t0,$t4
b here4

e2_is_less_dy:
add $t6,$t6,$t2
add $t1,$t1,$t5
b loop

exit:
lw $ra,0($sp)
addi $sp,$sp,4
jr $ra






user:
addi $sp,$sp,-4
sw $ra,0($sp)

#first message
li $v0,4
la $a0,read_input_message
syscall

#getting initial coordinates
li $v0,5
syscall

move $a1,$v0 #store initial x in a1

li $v0,5
syscall

move $a2,$v0 #store initial y in a2


jal MoveTo

#second message
li $v0,4
la $a0,read_input_message2
syscall

li $v0,5
syscall

move $a1,$v0 #store final x in a1

li $v0,5
syscall

move $a2,$v0 #store final y in a2

jal LineTo

li $v0,4
la $a0,final_message
syscall

lw $ra,0($sp)
addi $sp,$sp,4

jr $ra



